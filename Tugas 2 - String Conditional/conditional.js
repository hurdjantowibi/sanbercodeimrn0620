console.log()
console.log("Tugas Conditional / if-else")
console.log("---------------------------")

var nama = " "
var peran = " "

if (nama ==" ") {
  console.log("Nama harus diisi")
 } else {
  console.log(selesai)
}

console.log()

var nama = "John"
var peran = " "

if (nama =="John" || peran == " ") {
  console.log("Halo "+ nama + ",Pilih peranmu untuk memulai game!")
 } else {
  console.log(selesai)
}

console.log()

var nama = "Jane"
var peran = "Penyihir"

if (nama =="Jane" || peran == "Penyihir") {
  console.log("Selamat datang di Dunia Werewolf, "+ nama)
  console.log("Halo Penyihir "+ nama +", kamu dapat melihat siapa yang menjadi werewolf!")
 } else {
  console.log(selesai)
}

console.log()

var nama = "Jenita"
var peran = "Guard"

if (nama =="Jenita" || peran == "Guard") {
  console.log("Selamat datang di Dunia Werewolf, "+ nama)
  console.log("Halo " + peran +' '+ nama +", kamu akan membantu melindungi temanmu dari serangan werewolf")
 } else {
  console.log(selesai)
}

console.log()

var nama = "Junaedi"
var peran = "Warewolf"

if (nama =="Junaedi" || peran == "Warewolf") {
  console.log("Selamat datang di Dunia Werewolf, "+ nama)
  console.log("Halo " + peran +' '+ nama +", Kamu akan memakan mangsa setiap malam!")
 } else {
  console.log(selesai)
}


console.log()
console.log()
console.log("Tugas Conditional / Switch Case")
console.log("-------------------------------")

  var tanggal = 16;
  var bulan = 6;
  var tahun = 2020;
  switch(bulan) {
  case 1:   { console.log(tanggal +' Januari '+tahun); break; }
  case 2:   { console.log(tanggal +' Februari '+tahun); break; }
  case 3:   { console.log(tanggal +' Maret '+tahun); break; }
  case 4:   { console.log(tanggal +' April '+tahun); break; }
  case 5:   { console.log(tanggal +' Mei '+tahun); break; }
  case 6:   { console.log(tanggal +' Juni '+tahun); break; }
  case 7:   { console.log(tanggal +' Juli '+tahun); break; }
  case 8:   { console.log(tanggal +' Agustus '+tahun); break; }
  case 9:   { console.log(tanggal +' September '+tahun); break; }
  case 10:  { console.log(tanggal +' Oktober '+tahun); break; }
  case 11:  { console.log(tanggal +' November '+tahun); break; }
  case 12:  { console.log(tanggal +' Desember '+tahun); break; } 
  default:  { console.log(tanggal +' Tidak terjadi apa-apa '+tahun); }}