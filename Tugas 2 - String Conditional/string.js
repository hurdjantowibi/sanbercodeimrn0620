/* 
Soal No. 1 (Membuat kalimat) 
*/
console.log('Soal No.1')

var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

console.log(word.concat(' '+ second +' '+ third +' '+ fourth +' '+ fifth +' '+ sixth +' '+ seventh));

console.log()

/* 
Soal No. 2 Mengurai kalimat (Akses karakter dalam string) 
*/
console.log('Soal No.2')

var sentence = "I am going to be React Native Developer"; 

console.log('First Word: ' + sentence[0]); 
console.log('Second Word: ' + sentence[2] + sentence[3]); 
console.log('Third Word: ' + sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9]); 
console.log('Fourth Word: ' + sentence[11] + sentence[12]); 
console.log('Fifth Word: ' + sentence[14] + sentence[15]); 
console.log('Sixth Word: ' + sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21]); 
console.log('Seventh Word: ' + sentence[23] +sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28]); 
console.log('Eighth Word: ' + sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38])

console.log()

/* 
Soal No. 3 Mengurai Kalimat (Substring) 
*/
console.log('Soal No.3')

var sentence2 = 'wow JavaScript is so cool'; 

console.log('First Word: ' + sentence2.substring(0, 3)); 
console.log('Second Word: ' + sentence2.substring(4, 14)); 
console.log('Third Word: ' + sentence2.substring(15, 17)); 
console.log('Fourth Word: ' + sentence2.substring(18, 20)); 
console.log('Fifth Word: ' + sentence2.substring(21, 25));

console.log()

/* 
Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String 
*/
console.log('Soal No.4')

var sentence3 = 'wow JavaScript is so cool'; 

console.log('First Word: ' + sentence3.substring(0, 3) + ', with length: ' + sentence3.substring(0, 3).length); 
console.log('Second Word: ' + sentence3.substring(4, 14) + ',with length: ' + sentence3.substring(4, 14).length); 
console.log('Third Word: ' + sentence3.substring(15, 17) + ',with length: ' + sentence3.substring(15, 17).length); 
console.log('Fourth Word: ' + sentence3.substring(18, 20) + ',with length: ' + sentence3.substring(18, 20).length); 
console.log('Fifth Word: ' + sentence3.substring(21, 25) + ',with length: ' + sentence3.substring(21, 25).length);
console.log()

console.log()
console.log('Selesai. Terima kasih atas tugas Stringnya :)')